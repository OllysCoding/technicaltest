<?php

session_start();

//Only destroy the session if logged in; without this people who are locked out could use this to bypass the 5 minute wait time.
if ((isset($_SESSION['logged_in']) && isset($_SESSION['id']))) {
	session_destroy();
}



Header("Location: ../index.html");

?>
