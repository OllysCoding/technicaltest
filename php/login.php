<?php
session_start();
// ---- STATIC USER ----
// The is to simulate a database returning a user ID, username and password (encrypted with php sha256 method)
$query_result = [["id" => "1234",
				 "username" => "admin",
			   "password" => "6d4525c2a21f9be1cca9e41f3aa402e0765ee5fcc3e7fea34a169b1730ae386e"]]; //password is "admin_password"
// --- STATIC USER ---

//Ensure user isn't trying to spam login requests
if (isset($_SESSION['attempts'])) {
	if ($_SESSION['attempts'] > 4) {
		$secondsInactive = time() - $_SESSION['last_attempt'];
		if ($secondsInactive >= 300) {
			unset($_SESSION['attempts']);
			unset($_SESSION['last_attempt']);
		}
		else {
			echo 'blocked';
			die();
		}

	}
}

//It is always possible someone has manually posted an empty form, check the post values have been set.
if (isset($_POST['username']) && isset($_POST['password'])) {
	//XSS prevention is not needed as the data is only being selected;
	$username = mres($_POST['username']);

	$password = hash("sha256", $_POST['password']);


	//--- DATABASE ---
	// imaginary database call
	// query would be :
	// $query = "SELECT id, username FROM user_table WHERE username='".$username."' AND password='".$password."';"
	$user = $query_result[0];
	//--- DATABASE ---
	// Would be if using a database
	// if (count($query_result) == 1) {
	// required for login to work with sample user:
	if ($username == $user["username"] && $password == $user["password"]) {
		echo 'true';
		//set the session
		$_SESSION['id'] = $user["id"];
		$_SESSION['logged_in'] = true;


	}
	else {
		// up the attempts and set the last attempt to the current time. Uses a ternary statement to be a little more effecient.
		$_SESSION['attempts'] = (isset($_SESSION['attempts']) ? $_SESSION['attempts'] : 0) + 1;
		$_SESSION['last_attempt'] = time();

		echo 'false';
	}

}
else {
	echo 'false';
}

//custom anti sql injection function
//if a real database connection was used, mysqli_real_escape_string() could be used. However not possible without
//a database connection
function mres($value)
{
    $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
    $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");

    return str_replace($search, $replace, $value);
}



 ?>
