
<?php
session_start();


if (!(isset($_SESSION['logged_in']) && isset($_SESSION['id']))) {
	header('Location: index.html');
}


 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home - Dogfish</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
		<link rel="stylesheet" href="css/main.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  </head>
  <body class="has-navbar-fixed-top">
		<nav class="navbar is-fixed-top is-light" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
            <a class="navbar-item">Test Site</a>
            <button class="button navbar-burger" data-target="navMenu">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </div>


        <div class="navbar-menu" id="navMenu">
            <div class="navbar-start">

            </div>
            <div class="navbar-end">
                <a href="php/logout.php" class="navbar-item" id="login-modal-open">
                    Logout
                </a>
            </div>

        </div>
    </nav>


		<section class="hero is-dark is-fullheight">
		  <div class="hero-body">
		    <div class="container has-text-left">
		      <h1 class="title">
		        You're logged in!
		      </h1>
		      <h2 class="subtitle">
		        id=<?php echo $_SESSION["id"];?>
		      </h2>
		    </div>
		  </div>
		</section>



  </body>

	<script src="js/main.js"></script>


	</script>
</html>
