# README

This is the technical login test completed by Olly

### Installing

* Clone the Repository to your local machine
* Set up site onto a php enabled server (site tested on PHP 5.4.8)
  * Install instructions for a WAMP PHP Server:
  * Download the installer file from http://securewamp.org/en/download.php
  * Run the installer, leave all options as default; click install
  * After installation complete, run SecureWAMP
  * Click the "Install" option under the "Apache and PHP" subheading
  * Set "Document Root" to the main folder of the local git repo (folder containing index.html)
  * Click install
* Navigate to "localhost" in your browser

### Usage

* Login button is located at the top right, on mobile the dropdown burger is used
* Working username is "admin" and password "admin_password"


### How I met the requirements

##### UI Style

* The site uses the Bulma.io for it's CSS Framework. This is a light weight framework making it ideal for a small site like this, while providing excellent aesthetics and a consistent UI. However it lacks any built in javascript, increasing the time required as code for modals, messages boxes and buttons most be added manually. 
* Ajax is used for the login, and the form is stored on a modal on the homepage, keeping the UI clean, smooth and avoiding unnecessary page refreshes. 

#### Code Style
* If you're here then I've succeeded on this part!


#### Project Layout
* The project uses a fairly standard layout, javascript and css are kept in their separate files and folders. PHP files which are not URLs seen visibly by the user (login & logout) are also kept in their own folders and files. 


#### Security 

* Since the project wasn't using a database as suggested; some compromises were made on the SQL injection functions. mysqli functions could not be used as they require a connection variable. This is stated in the code comments along with other changes that would be needed to use an actual database. 
* Passwords are encrypted with the Sha256 algorithm 
* Users are locked out after 5 login attempts, with a timeout time of 5 minutes.
* The logout script will not allow bypassing of the login attempt block


#### Best Practice

* All the code although not fitting all the best practices in terms of readability, is well commented and uses sensible variable and class names.


#### Client Side Validation

* Before calling the AJAX, the fields are checked to ensure they're not empty, and an error is displayed through jquery showing which are empty. 


#### Mobile Optimisation

* The NavBar uses a burger dropdown when the width falls below a set value. The javascript for which is located in main.js
* Bulma CSS's modals are not optimised for mobile, so I implemented a media tag to resize it when the width falls below 600px. Without this the modal would go off the screen


#### Documentation

* All the documentation is in this readme. 


### My Approach 

I started out with the UI. I knew from experience that I wanted to use bulma, as it is fast to set up and also allows me to demonstrate my skills in JavaScript, which a more bulky framework such as bootstrap would not.
I deciced that I wanted to use a modal and Ajax for the client side login flow, since this kept the UX clean, smooth and avoided clunky page reloads. 
I wrote the javascript for the UI elements as I went along, finishing the client side of things off with the input validation and Ajax calls. 
I then set up a WAMP web server, documenting the steps I took to install it and get it started to be used for this readme. 
Writing the PHP I ran into an issues with PHP failing to do proper error reporting, so I had to take a break to install PHPStorm, which gave me some real time interpreting for errors in my PHP code. Up until this point I had been using Atom as an IDE.
Finishing up with some bugfixing and adding in the login attempts counter, I tested the login for a final time to be 100% sure it fully worked. 


#### Note

The site in it's current state uses CDN links for jQuery, bulma and font awesome. For a production build I would have used local versions and minified all the CSS and JS. 