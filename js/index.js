$(document).ready(function () {

	//Open and close the modal
	$('#login-modal-open').click(function () {
		$('.login-modal').fadeIn();
	});
	$('.login-modal-close').click(function () {
		if (!$('#login-form-submit').hasClass("is-loading")) {
			$('.login-modal').fadeOut(function() {
				//Reset the form once close is completed
				//Validation is not reset in a bid to remind the user what they did wrong upon opening modal a second time.

	     	$('#login-form').trigger("reset");


	  	});
		}
	});
	$('#login-error-close').click(function () {
		$('#login-error').fadeOut();
	});


	$("form").submit( function(e) {
            e.preventDefault(); // avoid to execute the actual submit of the form.

						//Set the button to loading, disable the closing buttons till this is complete.
            $('#login-form-submit').addClass('is-loading');
						$('.login-modal-close').attr("disabled", true);


						//Get the inputs
						var name = $('#username-input').val();
						var password = $('#password-input').val();


						//check to ensure both fields have received some data.
						var hasValidationError = false;
						if (name.length == 0) {
							$('#username-error-text').text("Please enter a username");
							$('.username-error').fadeIn();
							$('#username-input').addClass('is-danger');
							hasValidationError = true;
						}
						else {
							$('.username-error').fadeOut();
							$('#username-input').removeClass('is-danger');
						}
						if (password.length == 0) {
							$('#password-error-text').text("Please enter a password");
							$('.password-error').fadeIn();
							$('#password-input').addClass('is-danger');
							hasValidationError = true;
						}
						else {
							$('.password-error').fadeOut();
							$('#password-input').removeClass('is-danger');
						}

						if (hasValidationError) {
							$('#login-form-submit').removeClass('is-loading');
							$('.login-modal-close').attr("disabled", false);
							return;
						}

						//Run Ajax call to PHP script
            $.ajax({
                type: "POST",

								url: "php/login.php",
                cache:false,
                data: $("#login-form").serialize(), // serializes the form's elements.
                success: function(result)
                {
									//re-enable buttons after ajax is complete
									$('#login-form-submit').removeClass('is-loading');
									$('.login-modal-close').attr("disabled", false);


									//sort the result and display accordingly.
									if (result === "blocked") {
										$('#login-error-text').text("You have too many failed login attempts, try again in 5 minutes.");
										$('#login-error').fadeIn();
									}
									else if (result === "true") {
										window.location.href = "logged_in.php";
									}
									else {
										$('#login-error-text').text("Incorrect username/password combination.");
										$('#login-error').fadeIn();
									}


                },
								//something went wrong that. Check the readme to ensure everything meets the requirements. Feel free to email me if you cannot get it to work at all.
                error: function (xhr, ajaxOptions, thrownError) {
										$('#login-form-submit').removeClass('is-loading');
										$('.login-modal-close').attr("disabled", false);

										$('#login-error-text').text("An error occurred accessing the server, check readme to ensure you have your webserver set up correctly");
										$('#login-error').fadeIn();


                }
            });


        });
});
